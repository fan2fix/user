package com.fsg.user.entity;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import org.springframework.data.annotation.Id;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 用户文档实体类
 * @author lfanzhi
 */
@Data
//@Document(indexName = "vendor",type = "docs", replicas = 0, refreshInterval = "1m")
public class EsVendor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @ApiModelProperty(value = "唯一标识")
    private String id;

    @ApiModelProperty(value = "供应商名称")
    private String vendorName;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "联系人")
    private String contact;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "地区城市")
    private String city;

    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "公司税号")
    private String taxNo;

    @ApiModelProperty(value = "外部供应商ID")
    private String hostVendorId;

    @ApiModelProperty(value = "自定义文本1")
    private String cusText1;

    @ApiModelProperty(value = "自定义文本2")
    private String cusText2;

    @ApiModelProperty(value = "自定义文本3")
    private String cusText3;

    @ApiModelProperty(value = "自定义文本4")
    private String cusText4;

    @ApiModelProperty(value = "自定义文本5")
    private String cusText5;

    @ApiModelProperty(value = "自定义数值1")
    private BigDecimal cusNum1;

    @ApiModelProperty(value = "自定义数值2")
    private BigDecimal cusNum2;

    @ApiModelProperty(value = "自定义数值3")
    private BigDecimal cusNum3;

    @ApiModelProperty(value = "自定义数值4")
    private BigDecimal cusNum4;

    @ApiModelProperty(value = "自定义数值5")
    private BigDecimal cusNum5;

}

