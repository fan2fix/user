package com.fsg.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Builder;
import lombok.Data;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.util.Date;
import java.io.Serializable;

/**
 * <p>
 * 工单GPS
 * </p>
 *
 * @author lfz
 * @since 2020-05-15
 */

@Data
@Builder
@TableName("t_order_gps")
public class OrderGps implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 工单ID
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 经度
     */
    @TableField("gps_lng")
    private String gpsLng;

    /**
     * 纬度
     */
    @TableField("gps_lat")
    private String gpsLat;

    /**
     * 推送时间
     */
    @TableField("push_time")
    private Date pushTime;

    /**
     * 供应商订单编码
     */
    @TableField("isp_order")
    private String ispOrder;

    /**
     * 手机号码（接单司机）
     */
    @TableField("phone_no")
    private String phoneNo;

    /**
     * 状态信息ID值
     */
    @TableField("status_code")
    private String statusCode;

    /**
     * 速度
     */
    @TableField("speed")
    private String speed;

    /**
     * 方向
     */
    @TableField("direction")
    private String direction;

    /**
     * 精度
     */
    @TableField("accuracy")
    private String accuracy;

    /**
     * 设备号
     */
    @TableField("device_no")
    private String deviceNo;

    /**
     * 协议
     */
    @TableField("protocol")
    private String protocol;

    /**
     * 失效标志▲来源于sys_invalid_flag
     */
    @TableField("invalid_flag")
    private Boolean invalidFlag;

    /**
     * 创建人
     */
    @TableField("created_user")
    private String createdUser;

    /**
     * 创建时间
     */
    @TableField("created_time")
    private Date createdTime;

    /**
     * 修改人
     */
    @TableField("updated_user")
    private String updatedUser;

    /**
     * 修改时间
     */
    @TableField("updated_time")
    private Date updatedTime;

    @Override
    public String toString() {
        return "OrderGps{" +
        "id=" + id +
        ", orderId=" + orderId +
        ", gpsLng=" + gpsLng +
        ", gpsLat=" + gpsLat +
        ", pushTime=" + pushTime +
        ", ispOrder=" + ispOrder +
        ", phoneNo=" + phoneNo +
        ", statusCode=" + statusCode +
        ", speed=" + speed +
        ", direction=" + direction +
        ", accuracy=" + accuracy +
        ", deviceNo=" + deviceNo +
        ", protocol=" + protocol +
        ", invalidFlag=" + invalidFlag +
        ", createdUser=" + createdUser +
        ", createdTime=" + createdTime +
        ", updatedUser=" + updatedUser +
        ", updatedTime=" + updatedTime +
        "}";
    }
}
