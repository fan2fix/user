package com.fsg.user.entity;

import java.io.Serializable;
import java.util.Date;

//import org.springframework.data.elasticsearch.annotations.Document;
//import org.springframework.data.elasticsearch.annotations.Field;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
//@Document(indexName = "user",type = "docs", shards = 1, replicas = 0)
@TableName("fsg_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
   
    private String id;

    private String name;

    private String password;

    private String email;

    private String wechatNo;

    private String creator;

    private String userName;

    private String headPortrait;

    private Date createdTime;

    private Date modifiedTime;

    private String address;
    
//    public static String ID ="ID";
//
//    public static String NAME ="NAME";
//    
//    public static String PASS_WORD ="PASS_WORD";
//    
//    public static String EMAIL = "EMAIL";
//
//    public static String WECHAT_NO ="WECHAT_NO";
//
//    public static String CREATOR ="CREATOR";
//
//    public static String USER_NAME ="USER_NAME";
//    
//    public static String HEAD_PORTRAIT ="HEAD_PORTRAIT";
//
//    public static String CREATED_TIME ="CREATED_TIME";
//
//    public static String MODIFIED_TIME = "MODIFIED_TIME";
//    
//    public static String ADDRESS = "ADDRESS";
    
    public static void main(String[] args) {
		
	}
}