package com.fsg.user.service;

import com.fsg.user.entity.OrderGps;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 工单GPS 服务类
 * </p>
 *
 * @author lfz
 * @since 2020-05-15
 */
public interface OrderGpsService extends IService<OrderGps> {

}
