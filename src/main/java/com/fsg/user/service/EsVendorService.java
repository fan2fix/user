package com.fsg.user.service;



import com.fsg.user.entity.EsVendor;

/**
 * @author lfanzhi
 */
public interface EsVendorService {

    /**
     * 添加机构
     * @param esVendor
     * @return
     */
    EsVendor saveVendor(EsVendor esVendor);

    /**
     * 通过id删除机构
     * @param id
     */
    void deleteVendor(String id);

    /**
     * 删除全部机构
     */
    void deleteAll();

    /**
     * 分页搜索获取机构
     * @param type
     * @param key
     * @param searchVo
     * @param pageable
     * @return
     */
   // Page<EsVendor> findByCondition(Integer type, String key, SearchVo searchVo, Pageable pageable);
}
