package com.fsg.user.service;

import com.fsg.user.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lfz
 * @since 2020-05-01
 */
public interface UserService extends IService<User> {

}
