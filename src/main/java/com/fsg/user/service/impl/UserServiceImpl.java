package com.fsg.user.service.impl;

import com.fsg.user.entity.User;
import com.fsg.user.mapper.UserMapper;
import com.fsg.user.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lfz
 * @since 2020-05-01
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
