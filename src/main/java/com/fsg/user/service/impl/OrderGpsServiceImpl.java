package com.fsg.user.service.impl;

import com.fsg.user.entity.OrderGps;
import com.fsg.user.mapper.OrderGpsMapper;
import com.fsg.user.service.OrderGpsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 工单GPS 服务实现类
 * </p>
 *
 * @author lfz
 * @since 2020-05-15
 */
@Service
public class OrderGpsServiceImpl extends ServiceImpl<OrderGpsMapper, OrderGps> implements OrderGpsService {

}
