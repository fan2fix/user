package com.fsg.user.model;


import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class UploadFileReq {
	
	private MultipartFile file;
	
	private String fileEncodeStr;
	
	private String fileName;

}