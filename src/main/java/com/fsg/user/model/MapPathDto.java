package com.fsg.user.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class MapPathDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String name;
	
	Double[][] path;
}
