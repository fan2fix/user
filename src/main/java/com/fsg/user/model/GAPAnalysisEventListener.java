package com.fsg.user.model;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.fsg.user.service.UserService;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Getter
public class GAPAnalysisEventListener extends AnalysisEventListener<GAPDemo>  {
	@Autowired
	UserService userService;

	List<GAPDemo>  gAPDemoList;

	ExcelReadInfo excelReadInfo;

	@Override
	public void invoke(GAPDemo data, AnalysisContext context) {
		//调用方法
		//GAPController.invokeMethod();
		//excelReadInfo.
		log.info("解析数据{},{}",data,userService.count());
		excelReadInfo.setSheetName(context.getCurrentSheet().getSheetName());
		//错误信息
		data.setErrInfo("思思 小猪蹄");
		excelReadInfo.setIsHasErr(true);
		gAPDemoList.add(data);
	}
	@Override
	public void doAfterAllAnalysed(AnalysisContext context) {
		log.info("解析数据完成{}",gAPDemoList);
		userService.count();
	}
	
	


}
