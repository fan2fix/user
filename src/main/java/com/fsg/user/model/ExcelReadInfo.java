package com.fsg.user.model;

import lombok.Data;

@Data
public class ExcelReadInfo {
	
	private Boolean isHasErr =false;
	
	private String sheetName;

}
