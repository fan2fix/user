package com.fsg.user.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;

import com.alibaba.excel.annotation.write.style.HeadStyle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.apache.poi.ss.usermodel.FillPatternType;

@Data
@AllArgsConstructor
@NoArgsConstructor
//头背景设置成红色 IndexedColors.RED.getIndex()
@HeadStyle(fillPatternType = FillPatternType.NO_FILL)
public class GAPDemo implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ExcelProperty(value = "行驶证车主姓名")
    private String personnelName;

    @ExcelProperty(value = "证件类型")
    private String certificateType;

    @ExcelProperty(value = "证件号码")
    private String certificateCode;

    @ExcelProperty(value = "车辆情况")
    private String vehicleSituation;

    @ExcelProperty(value = "所属性质")
    private String ownerAttribute;

    @ExcelProperty(value = "车架号")
    private String vehicleFrameNo;

    @ExcelProperty(value = "车辆品牌")
    private String vehicleBrand;

    @ExcelProperty(value = "车型标准名称")
    private String standardName;

    @ExcelProperty(value = "核定座位")
    private String vehicleSeats;

    @NumberFormat("#.##")
    @ExcelProperty(value = "新车购置价")
    private BigDecimal purchasePrice;

    @NumberFormat("#.##")
    @ExcelProperty(value = "车辆购置税")
    private BigDecimal purchaseTax;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value = "初次登记日期")
    private Date firstRegisterData;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value = "车辆购买日期")
    private Date purchaseDate;

    @NumberFormat("#.##")
    @ExcelProperty(value = "保险金额")
    private BigDecimal insuredAmount;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value = "起保日期")
    private Date effectiveDate;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value = "到期日期")
    private Date expireDate;

    @ExcelProperty(value = "是否有贷款")
    private String loanFlag;

    @ExcelProperty(value = "项目类型")
    private String projectType;
    
    @ExcelProperty(value = "错误信息")
    private String errInfo;
    

}
