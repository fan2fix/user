package com.fsg.user;


import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fsg.user.config.stream.logtest.MyProcessor;
import com.fsg.user.config.stream.usermq.TopicUserSource;


@SpringBootApplication
@EnableEurekaClient
@EnableHystrixDashboard
@EnableBinding(value = {MyProcessor.class,TopicUserSource.class})
@MapperScan(basePackages = {"com.fsg.user.mapper"}) 
public class UserApplication {

	public static void main(String[] args) {
		System.setProperty("es.set.netty.runtime.available.processors", "false");
		SpringApplication.run(UserApplication.class, args);
	}
	
	@Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));// 日期格式化输出
        mapper.setTimeZone(TimeZone.getDefault());
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);// 忽略未知属性
        mapper.setSerializationInclusion(Include.NON_NULL);
        return mapper;
    }
	
	@Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
    }
}
