package com.fsg.user.mapper.elastic;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
//import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.fsg.user.entity.EsVendor;


/**
 * @author Exrickx
 */
public interface EsVendorDao {//extends ElasticsearchRepository<EsVendor, String> {

    /**
     * 通过类型获取
     * @param type
     * @param pageable
     * @return
     */
    Page<EsVendor> findByVendorName(String vendorName, Pageable pageable);
}
