package com.fsg.user.mapper;

import com.fsg.user.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lfz
 * @since 2020-05-01
 */
public interface UserMapper extends BaseMapper<User> {

}
