package com.fsg.user.mapper;

import com.fsg.user.entity.OrderGps;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 工单GPS Mapper 接口
 * </p>
 *
 * @author lfz
 * @since 2020-05-15
 */
public interface OrderGpsMapper extends BaseMapper<OrderGps> {

}
