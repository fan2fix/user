package com.fsg.user.provider;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fsg.user.entity.OrderGps;
import com.fsg.user.model.MapPathDto;
import com.fsg.user.service.OrderGpsService;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.ResourceUtils;

/**
 * <p>
 * 工单GPS 前端控制器
 * </p>
 *
 * @author lfz
 * @since 2020-05-15
 */
@Controller
@RequestMapping("/orderGps")
public class OrderGpsController {

	@Autowired
	OrderGpsService orderGpsService;

	@GetMapping("/map")
	public ModelAndView delIndex(String address) {
		ModelAndView modelAndView = new ModelAndView("map");
		modelAndView.addObject("address", address);
		return modelAndView;
	}

	@GetMapping("/parent")
	public String parent() {
		return "parent";

	}

	@GetMapping("/children")
	public String child() {
		return "children";
	}

	@ResponseBody
	@PostMapping(value ="initMapData", produces = { "application/json;charset=UTF-8" })
	public MapPathDto initMapData() {
		MapPathDto mapPathDto= this.parseJsonFile();
		Double[][] path =mapPathDto.getPath();
		if(ObjectUtils.isEmpty(path)) {
			return mapPathDto;
		}
		Date data= new Date();
		List<OrderGps> orderGpsList =new ArrayList<>();
		for (Double[] coordinate : path) {
			OrderGps orderGps =OrderGps.builder().orderId(520L).gpsLng(String.valueOf(coordinate[0])).gpsLat(String.valueOf(coordinate[1])).pushTime(data)
					.ispOrder("IspOrder").phoneNo("PhoneNo").statusCode("statusCode").speed("speed").direction("direction")
					.accuracy("accuracy").deviceNo(mapPathDto.getName()).protocol("protocol").invalidFlag(false).createdUser("fanfan")
					.createdTime(data).updatedUser("sisi").updatedTime(data).build();
			data =DateUtils.addSeconds(data, 5);//datatime.plusSeconds(5);
			orderGpsList.add(orderGps);
		}
		if(CollectionUtils.isEmpty(orderGpsList)) {
			return mapPathDto;
		}
		boolean saveFlag =orderGpsService.saveBatch(orderGpsList);
		if(!saveFlag) {
			//to do log 保存失败
		}
		return mapPathDto;
	}


	@ResponseBody
	@GetMapping(value ="mapPath", produces = { "application/json;charset=UTF-8" })
	public MapPathDto[] getMapPath(@RequestParam String orderId) {
		MapPathDto[] mapPathDtos =new MapPathDto[1];
		MapPathDto mapPathDto =new MapPathDto();
		QueryWrapper<OrderGps> orderGpsQueryWrapper =new QueryWrapper<>();
		orderGpsQueryWrapper.eq("order_id", orderId);
		orderGpsQueryWrapper.orderByAsc("push_time");
		List<OrderGps> orderGpsList= orderGpsService.list(orderGpsQueryWrapper);
		if(CollectionUtils.isEmpty(orderGpsList)) {
			return mapPathDtos;
		}
		mapPathDto.setName(orderGpsList.get(0).getDeviceNo());
		Double[][] path =new Double[orderGpsList.size()][2];

		for (int i=0;i<orderGpsList.size();i++) {
			path[i][0]=Double.valueOf(orderGpsList.get(i).getGpsLng());
			path[i][1]=Double.valueOf(orderGpsList.get(i).getGpsLat());
		}
		mapPathDto.setPath(path);
		mapPathDtos[0] =mapPathDto;
		return mapPathDtos;
	}

	public  MapPathDto parseJsonFile(){
		//JsonMapper jm =new JsonMapper();
		MapPathDto mapPathDto = new MapPathDto();
//		try {
//			File f=	ResourceUtils.getFile("/Users/lfz/git/user/src/main/resources/static/json/map(1).json");
//			//mapPathDto = jm.readValue(f, MapPathDto.class);
//			System.err.println("点数{}"+ (ObjectUtils.isEmpty(mapPathDto.getPath())?0: mapPathDto.getPath().length));
//		} catch (JsonParseException e) {
//			//to do log JSON 解析异常
//		} catch (JsonMappingException e) {
//			//to do log JSON 解析异常
//		}  catch (IOException e) {
//			//to do log 文件解析异常
//		}
		return mapPathDto;
	}


}

