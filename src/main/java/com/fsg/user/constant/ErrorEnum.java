package com.fsg.user.constant;


public enum ErrorEnum {
	
		// 参数校验错误可自行定义, 0 开头
		/*0 :参数名称 ，1参数字段名*/
		PARAM_CHECK_NOTPASS_2("0001","{0}{1}校验不通过,请重新输入"),
		PARAM_CHECK_NOTEMPTY_2("0002","{0}{1}为空,请重新输入"),
		PARAM_CHECK_NOTEMPTY("0003","{0}为空,请重新输入"),
		// 系统错误可自行定义, 1 开头
		OPRATE_EEROR("1001","操作失败,请重试"),
		IMG_UPLOADEEROR("1001","图片上传失败,请重试"),
		// 业务错误，2开头
		LOGIN_NOTPASS("2001","用户名或密码错误,请重新输入"),
		REGISTER_NOTPASS("2002","注册失败,请重试"),
		VERIFICODE_NOTPASS("2004","验证码输入错误"),
		ADDERROR_EXISTNAME("2003","新增失败,{0}名已存在"),
		// db数据库业务操作错误, 3开头
		INSERT_FAIL("3001","新增{0}失败,请稍后再试"),
		UPDATE_FAIL("3002","更新{0}失败,请稍后再试"),
		SERACH_FAIL("3003","查询{0}失败,请稍后再试"),
		DELETE_FAIL("3004","删除{0}失败,请稍后再试"),
		SAVE_FILEEEROR("3005","保存{0}失败,请重试"),
		CREATE_FAIL("3006","生成{0}失败,请稍后再试"),
	;
	
	private String code;

	private String defaultMessage;

	ErrorEnum(String code, String defaultMessage) {
		this.code = code;
		this.defaultMessage = defaultMessage;
	}

	public String getErrorCode() {
		return this.code;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}
	
	
}
