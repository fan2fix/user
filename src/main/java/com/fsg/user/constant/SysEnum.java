package com.fsg.user.constant;

public class SysEnum {
	
	public final static long ONE_MINUTE =60*1000;
	public final static long HALF_MINUTE =30*1000;
	
	public  static String YES ="1";
	public  static String NO ="0";
	
	public  static String USER ="用户";
	public  static String VIDEO ="视频";
	public  static String ORDER ="订单";
	
	
	public  static String GROUP_ORDER ="GD"; //团购订单
	public  static String NORMAL_ORDER ="D";//订单
	
	
	public  static String UNKOWN ="未知";//未知
	
	
	/**
	 * 借款状态  
	 * @author user
	 *
	 */
	public enum GroupOrderStatus {
		CREATED("created","已创建"), 
		START("start","开始"),
		MAJORITY("majority","热销"),
		FULLED("fulled","满员"),
		PROCESSING("processing","处理中"),
		COMPLETED("completed","已完成");
		private String code;
		private String value;
		GroupOrderStatus(String code,String value) {
			this.value = value;
			this.code = code;
		}
		public String getValue() {
			return value;
		}
		public String getCode() {
			return code;
		}
		public String getValueByCode(String code) {
			for (GroupOrderStatus groupOrderStatus :GroupOrderStatus.values() ) {
				if(groupOrderStatus.value.equals(code)){
					return groupOrderStatus.value;
				}
			}
			return UNKOWN;
		}
	}

	
}
