package com.fsg.user.config.stream.usermq;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.stereotype.Component;

@MessagingGateway
@Component
public interface TopicUserGateway {

	@Gateway(requestChannel = TopicUserSource.CHANNEL_TOPIC_USER_REGISTER_OUTPUT)
	void  sendRegisterMsg(String message);
	
	
	@Gateway(requestChannel = TopicUserSource.CHANNEL_TOPIC_USER_LOGIN_OUTPUT)
	void  sendLoginMsg(String message);
}
