package com.fsg.user.config.stream.usermq;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface TopicUserSource {

	String CHANNEL_TOPIC_USER_REGISTER_OUTPUT = "topicUserRegisterOutPut";
	
	
	String CHANNEL_TOPIC_USER_LOGIN_OUTPUT = "topicUserLoginOutPut";
	
	/**
	 * 功能说明: 
	 * 最后修改时间:
	 * @return
	 */
	@Output(CHANNEL_TOPIC_USER_REGISTER_OUTPUT)
	MessageChannel topicUserRegisterChannel();
	
	
	/**
	 * 功能说明: 
	 * 最后修改时间:
	 * @return
	 */
	@Output(CHANNEL_TOPIC_USER_LOGIN_OUTPUT)
	MessageChannel topicUserLoginChannel();
	
	
}
