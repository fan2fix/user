package com.fsg.user.config.config;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.ParseException;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author lfz
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {
	
	@Override
	public void addFormatters(FormatterRegistry registry) {

		registry.addFormatterForFieldType(String.class, new Formatter<String>() {
			@Override
			public String parse(String text, Locale locale) throws ParseException {
				if(StringUtils.isBlank(text)){
					return null;
				}else {
					return text;
				}
			}

			@Override
			public String print(String object, Locale locale) {
				return object;
			}
		});
	}
	
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/**");
//	}
}