package com.fsg.user.config.exception;

import java.text.MessageFormat;


import com.fsg.user.constant.ErrorEnum;


public class BizException extends Exception  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected ErrorEnum errorCode;
	
	protected String errorMsg;
	
	protected String[] arguments;
	
	protected String url;

	
	public ErrorEnum getErrorCode() {
		return errorCode;
	}
	public BizException() {
	}

	public BizException(ErrorEnum ErrorEnum, String... arguments) {
		this.errorCode = ErrorEnum;
		this.arguments =arguments;
	}
	
	public String getMessage() {
		
		String notMessage = "not error, not message";
		String defaultMessage = "";
		
		if(errorMsg !=null){
			defaultMessage = errorMsg;
		}else{
			if (errorCode != null ) {
				defaultMessage = errorCode.getDefaultMessage();
			}
		}		
		if (defaultMessage==null||defaultMessage.trim() =="") {
			return notMessage;
		}
		return MessageFormat.format(defaultMessage, this.arguments);
	}

	
}
