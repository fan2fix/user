package com.fsg.user.config.elastic;

import java.util.List;

//import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.fsg.user.entity.User;


public interface UserRepository {//extends ElasticsearchCrudRepository<User,Long> {
	
	List<User> findByAddress(String address);
	
	List<User> findByAddressLike(String address);
	
	List<User> findByName(String name);

}