var layer;
layui.use(['layer', 'form','util'], function(){
	  layer = layui.layer
	  ,form = layui.form;
	  var util = layui.util;
	  //示例
	  var endTime = new Date(2019,05,21).getTime() //假设为结束日期
	  ,serverTime = new Date().getTime(); //假设为当前服务器时间，这里采用的是本地时间，实际使用一般是取服务端的
	   
	  util.countdown(endTime, serverTime, function(date, serverTime, timer){
	    var str = date[0] + '天' + date[1] + '时' +  date[2] + '分' + date[3] + '秒';
	    layui.$('#test').html('距离2019年05月21日还有：'+ str);
	  });
	});
$('#register-button').click(function (event) {
	var name =$("#name").val();
	var password =$("#password").val(); 
	var userName =$("#userName").val();
	var data ={};
	data.name =name;
	data.password =password;
	data.userName =userName;
	$.ajax({
 		    //几个参数需要注意一下
		        type: "POST",//方法类型
		        dataType: "json",//预期服务器返回的数据类型
		        data: JSON.stringify(data),
		        contentType:"application/json",
		        url: "/user/register", //url
		        success: function (result) {
		           //打印服务端返回的数据(调试用)
		            if (result.code == '0000') {
		                $('form').fadeOut(200);
		            	$('.wrapper').addClass('form-success');
		            	layer.msg("注册成功！");
		            	location.href="/index";
		            }else{
		            	console.log(result.msg);
		            	layer.msg(result.msg);
		            };
		        },
		        error : function(result) {
		        	layer.msg(result.msg);
		        }
		    }); 
	
});